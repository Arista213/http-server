FROM maven

ADD . /app

WORKDIR /app

EXPOSE 8080

RUN mvn clean test package install

ENTRYPOINT [ "java","-jar","/app/target/HttpServer-1.0-SNAPSHOT.jar" ]