import handlers.IHandler;
import handlers.SumHandler;
import org.junit.Assert;
import org.junit.Test;
import servers.TcpServer;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import static config.Configuration.PATH_TO_FILES;

public class HttpServerTests
{
    private static Socket socket;
    private static InputStream is;
    private static OutputStream os;
    private static Thread thread;
    private static TcpServer tcpServer;
    private static boolean isRunning;

    private static void startTcpServer()
    {
        if (!isRunning)
        {
            IHandler handler = new SumHandler();
            tcpServer = new TcpServer(handler);
            thread = new Thread(() -> tcpServer.start(8080));
            thread.start();
            isRunning = true;
        }
    }

    @Test
    public void tcpServerTest() throws IOException, InterruptedException
    {
        startServer();
        stopServer();
    }

    @Test
    public void badRequestTest() throws IOException, InterruptedException
    {
        startServer();

        String request = """
                GET /UNKNOWN_FILE.txt HTTP/1.1
                                
                """;

        String expected = """
                HTTP/1.1 404 Not Found
                Content-Type: text/text; charset=UTF-8
                Content-Length: 0
                """;

        os.write(request.getBytes(StandardCharsets.UTF_8));
        String actual = new String(is.readAllBytes(), StandardCharsets.UTF_8);

        stopServer();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void goodRequestTest() throws IOException, InterruptedException
    {
        startServer();

        String request = """
                GET /file1.txt HTTP/1.1
                                
                """;

        String expected = """
                HTTP/1.1 200 OK
                Content-Type: text/text; charset=UTF-8
                Content-Length: 2
                                
                35
                """;

        os.write(request.getBytes(StandardCharsets.UTF_8));
        String actual = new String(is.readAllBytes(), StandardCharsets.UTF_8);

        stopServer();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void PipelineTest() throws IOException, InterruptedException
    {
        startServer();
        createFile("test1.txt", "1 2 3 4");
        createFile("test2.txt", "10 10 10 10\n 10 10");
        createFile("test3.txt", "1 1\n\n 1 1");

        String request = """
                GET /test1.txt HTTP/1.1
                Connection: keep-alive
                               
                GET /test2.txt HTTP/1.1
                Connection: keep-alive
                               
                GET /test3.txt HTTP/1.1
                         
                """;

        String expected = """                
                HTTP/1.1 200 OK
                Content-Type: text/text; charset=UTF-8
                Content-Length: 2
                               
                10
                                
                HTTP/1.1 200 OK
                Content-Type: text/text; charset=UTF-8
                Content-Length: 2
                               
                60
                                
                HTTP/1.1 200 OK
                Content-Type: text/text; charset=UTF-8
                Content-Length: 1
                               
                4
                """;

        os.write(request.getBytes(StandardCharsets.UTF_8));
        String actual = new String(is.readAllBytes(), StandardCharsets.UTF_8);

        stopServer();
        deleteFile("test1.txt");
        deleteFile("test2.txt");
        deleteFile("test3.txt");
        Assert.assertEquals(expected, actual);

    }


    private void startServer() throws IOException, InterruptedException
    {
        startTcpServer();
        while (true)
        {
            try
            {
                socket = new Socket("localhost", 8080);
                break;
            }
            catch (java.net.BindException e)
            {
                Thread.sleep(100);
            }
        }

        os = socket.getOutputStream();
        is = socket.getInputStream();
    }

    private void stopServer() throws IOException
    {
        is.close();
        os.close();
        socket.close();
        thread.interrupt();
    }

    private void createFile(String filename, String content) throws IOException
    {
        File file = new File(PATH_TO_FILES + filename);
        OutputStream os = new FileOutputStream(file);
        os.write(content.getBytes(StandardCharsets.UTF_8));
        os.flush();
        os.close();
    }

    private void deleteFile(String filename) throws IOException
    {
        File file = new File(PATH_TO_FILES + filename);
        if (!file.delete()) throw new IOException();
    }
}
