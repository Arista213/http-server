import entities.Response;
import org.junit.Assert;
import org.junit.Test;

public class ResponseTest
{
    @Test
    public void responseToStringTest()
    {
        Response response = new Response(200, "23");
        String expected = """
                HTTP/1.1 200 OK
                Content-Type: text/text; charset=UTF-8
                Content-Length: 2
                                
                23
                """;
        String actual = response.toString();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void badResponseToStringTest() {
        Response response = new Response(404, "");
        String expected = """
                HTTP/1.1 404 Not Found
                Content-Type: text/text; charset=UTF-8
                Content-Length: 0
                """;
        String actual = response.toString();
        Assert.assertEquals(expected, actual);
    }
}
