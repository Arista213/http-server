package config;

import java.io.File;

public enum Configuration
{
    PATH_TO_FILES("src" + File.separator + "main" + File.separator + "resources" + File.separator);

    private final String path;

    Configuration(String path)
    {
        this.path = path;
    }

    @Override
    public String toString()
    {
        return path;
    }
}
