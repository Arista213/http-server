package entities;

import java.util.Locale;

public enum Method
{
    GET,
    POST,
    PUT,
    DELETE;

    public static Method Parse(String method)
    {
        return switch (method.toLowerCase(Locale.ROOT))
                {
                    case "get" -> GET;
                    case "post" -> POST;
                    case "pur" -> PUT;
                    case "delete" -> DELETE;
                    default -> throw new IllegalArgumentException();
                };
    }
}
