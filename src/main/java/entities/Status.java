package entities;

public enum Status
{
    OK(200, "OK"),
    NotFound(404, "Not Found"),
    InternalServerError(500, "Internal Server Error");

    private final int status;
    private final String code;

    Status(int status, String code)
    {
        this.status = status;
        this.code = code;
    }

    public int getValue()
    {
        return status;
    }

    public String getCode()
    {
        return code;
    }

    public static Status parse(int status)
    {
        return switch (status)
                {
                    case 200 -> Status.OK;
                    case 500 -> InternalServerError;
                    default -> Status.NotFound;
                };
    }
}
