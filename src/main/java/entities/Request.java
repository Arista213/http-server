package entities;

public class Request
{
    private final Method method;
    private final String resource;
    private final String protocol;
    private final boolean keepAlive;

    public Request(Method method, String resource, String protocol, boolean keepAlive)
    {

        this.method = method;
        this.resource = resource;
        this.protocol = protocol;
        this.keepAlive = keepAlive;
    }

    public Method getMethod()
    {
        return method;
    }

    public String getProtocol()
    {
        return protocol;
    }

    public boolean isKeepAlive()
    {
        return keepAlive;
    }

    public String getResource()
    {
        return resource;
    }
}
