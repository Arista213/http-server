package entities;


public class Response
{
    private final Status status;
    private final String body;

    public Response(int status, String body)
    {
        this.status = Status.parse(status);
        this.body = body;
    }

    private String getHeader()
    {
        return String.format("""
                HTTP/1.1 %d %s
                Content-Type: text/text; charset=UTF-8
                Content-Length: %d
                """, status.getValue(), status.getCode(), body.length());
    }

    private String getBody()
    {
        return "\n" + body + "\n";
    }

    @Override
    public String toString()
    {
        if (body.isEmpty())
        {
            return getHeader();
        }
        return getHeader() + getBody();
    }
}
