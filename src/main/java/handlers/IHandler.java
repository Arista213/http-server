package handlers;

import entities.Request;
import entities.Response;

public interface IHandler
{
    Response Handle(Request request);
}
