package handlers;

import entities.Request;
import entities.Response;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static config.Configuration.PATH_TO_FILES;

public class SumHandler implements IHandler
{

    @Override
    public Response Handle(Request request)
    {
        Path path = Path.of(PATH_TO_FILES + request.getResource());
        try
        {
            String[] content = Files.readString(path).split("\\s+");
            int sum = Sum(content);
            return new Response(200, Integer.toString(sum));
        }
        catch (IOException e)
        {
            return new Response(404, "");
        }
    }

    private static int Sum(String[] content) throws IOException
    {
        int sum = 0;

        for (String word : content)
        {
            try
            {
                sum += Integer.parseInt(word);
            }
            catch (NumberFormatException ignored)
            {
            }
        }

        return sum;
    }
}
