import handlers.IHandler;
import handlers.SumHandler;
import servers.TcpServer;

public class Main
{
    public static void main(String[] args)
    {
        IHandler handler = new SumHandler();
        TcpServer tcpServer = new TcpServer(handler);
        tcpServer.start(8080);
    }
}