package servers;

import entities.Method;
import entities.Request;
import entities.Response;
import handlers.IHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class TcpServer {
    private final IHandler handler;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private BufferedReader in;
    private OutputStream os;

    public TcpServer(IHandler handler) {
        this.handler = handler;
    }

    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                clientSocket = serverSocket.accept();
                os = clientSocket.getOutputStream();
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                try {
                    listen();
                } catch (SocketException ignored) {
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void listen() throws IOException {
        boolean keepAlive = true;

        while (keepAlive) {
            List<String> input = new ArrayList<>();
            HashMap<String, String> requestHeader = new HashMap<>();

            String line = in.readLine();

            if (line == null) {
                keepAlive = false;
                continue;
            }

            while (!line.isEmpty()) {
                input.add(line);
                line = in.readLine();
            }

            String[] firstLine = input.get(0).split(" ");

            input.stream().skip(1).forEach(field ->
            {
                String[] splitField = field.split(": ");
                requestHeader.put(splitField[0], splitField[1]);
            });

            Method method = Method.Parse(firstLine[0]);
            String resource = firstLine[1].replace("/", "");
            String protocol = firstLine[2];
            String connection = requestHeader.get("Connection");
            keepAlive = Objects.equals(connection, "keep-alive");

            Request request = new Request(method, resource, protocol, keepAlive);
            Response response = handler.Handle(request);
            os.write(response.toString().getBytes(StandardCharsets.UTF_8));
            if (keepAlive) os.write((byte) '\n');
        }

        in.close();
        os.close();
        clientSocket.close();
    }
}
