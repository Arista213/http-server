# HTTP-Server

___

#### Считает сумму чисел в текстовом файле из корневой директории.

#### `localhost:8080/<filename>`

### Запуск из докера.

#### Собрать

#### `docker build -t server .`

#### Запустить

#### `docker run --network=host server`

##### Для примера есть файлы:

- file1.txt
- file2.txt
- file3.txt